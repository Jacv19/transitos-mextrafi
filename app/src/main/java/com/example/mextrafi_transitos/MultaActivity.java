package com.example.mextrafi_transitos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MultaActivity extends AppCompatActivity {

    private TextView txtMatricula;
    private Button btnMultar;
    private String matricula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multa);

        txtMatricula = (TextView) findViewById(R.id.txtMatricula);
        btnMultar = (Button) findViewById(R.id.btnMultar);

        btnMultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean completo = true;
                if(txtMatricula.getText().toString().equals("")) {
                    txtMatricula.setError("Introduce la matricula");
                    completo = false;
                }
                if(completo){
                    matricula = txtMatricula.getText().toString();
                    Intent intent = new Intent(getApplicationContext(),CapturarMultaActivity.class);
                    intent.putExtra("matricula",matricula); ////Para enviar dato a otra actividad
                    intent.putExtras(intent);
                    startActivity(intent);
                }
            }
        });

    }

    //Metodo para mostrar y ocultar menu
    public  boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.overflowperfil, menu);

        return true;
    }

    //Metodo para asignar funciones al menu
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.perfilItem1) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
