package com.example.mextrafi_transitos;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mextrafi_transitos.Objetos.Notas;
import com.example.mextrafi_transitos.Objetos.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaNotasActivity extends ListActivity implements
        Response.Listener<JSONObject>, Response.ErrorListener {

    private Button btnNuevo;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Notas> listaNotas;
    private String serverip = "https://mextrafi.leandevware.com.mx/";
    private int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_notas);

        //Sacar el dato String
        Bundle datos = getIntent().getExtras();
        idUser = datos.getInt("idUsuario");

        Toast.makeText(getApplicationContext(), "idUser: "+idUser, Toast.LENGTH_SHORT).show();

        listaNotas = new ArrayList<Notas>();
        request = Volley.newRequestQueue(context);

        wsNotasConsultar();

        btnNuevo = (Button) findViewById(R.id.btnNuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    public void wsNotasConsultar() {
        String url = serverip + "wsNotasConsultar.php?idUser=" + idUser;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(ListaNotasActivity.this, "regreso: "+response, Toast.LENGTH_SHORT).show();
        Notas notas = null;
        JSONArray json = response.optJSONArray("notas");
        try {
            for(int i=0; i<json.length(); i++) {
                notas = new Notas();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);

                notas.setID(jsonObject.optInt("id_notas"));
                notas.setTitulo(jsonObject.optString("titulo"));
                notas.setContenido(jsonObject.optString("contenido"));
                notas.setFecha(jsonObject.optString("fecha"));
                notas.setFavorite(jsonObject.optInt("favorite"));
                notas.setIdUser(jsonObject.optInt("usuario_id"));
                listaNotas.add(notas);
            }
            final MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.layout_notas, listaNotas);
            setListAdapter(adapter);
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyArrayAdapter extends ArrayAdapter<Notas> {
        Context context;
        int textViewRecursoId;
        ArrayList<Notas> objects;

        public MyArrayAdapter(Context context, int textViewResourceId,
                              ArrayList<Notas> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup
                viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater)
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView lblTitulo = (TextView) view.findViewById(R.id.lblTituloNota);
            TextView lblFecha = (TextView) view.findViewById(R.id.lblfechaNota);
            ImageButton btnModificar = (ImageButton) view.findViewById(R.id.btnModificarNota);
            ImageButton btnBorrar = (ImageButton) view.findViewById(R.id.btnBorrarNota);

            if (objects.get(position).getFavorite() > 0) {
                lblTitulo.setTextColor(Color.BLUE);
                lblFecha.setTextColor(Color.BLUE);
            } else {
                lblTitulo.setTextColor(Color.BLACK);
                lblFecha.setTextColor(Color.BLACK);
            }
            lblTitulo.setText(objects.get(position).getTitulo());
            lblFecha.setText(objects.get(position).getFecha());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    php.setContext(context);
                    Log.i("id", String.valueOf(objects.get(position).getID()));
                    php.borrarNotas(objects.get(position).getID());
                    objects.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Contacto eliminado con exito", Toast.LENGTH_SHORT).show();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("notas", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }

    //Metodo para mostrar y ocultar menu
    public  boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.overflowperfil, menu);

        return true;
    }

    //Metodo para asignar funciones al menu
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.perfilItem1) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
