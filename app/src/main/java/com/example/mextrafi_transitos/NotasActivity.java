package com.example.mextrafi_transitos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mextrafi_transitos.Objetos.Notas;
import com.example.mextrafi_transitos.Objetos.ProcesosPHP;
import com.example.mextrafi_transitos.Objetos.Usuarios;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NotasActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>
        ,Response.ErrorListener{

    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private TextView txtTitulo;
    private TextView txtContenido;
    private CheckBox cbkFavorite;
    private Notas savedNotas;
    ProcesosPHP php;
    private int id, idUser;
    private String fecha, usuario, password;

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas);

        request = Volley.newRequestQueue(NotasActivity.this);
        Date d = new Date();
        SharedPreferences preferences = getSharedPreferences("preferenciasLogin", Context.MODE_PRIVATE);
        usuario = preferences.getString("usuario", "");
        password = preferences.getString("password", "");

        //Toast.makeText(NotasActivity.this, "Exito: "+password+usuario,Toast.LENGTH_SHORT).show();
        consultarUsuarioWebService();

        //SACAMOS LA FECHA COMPLETA
        SimpleDateFormat fecc=new SimpleDateFormat("yyyy-MM-dd");
        fecha = fecc.format(d);

        initComponents();
        setEvents();

    }

    @Override
    public void onClick(View v) {
        if(isNetworkAvailable()) {
            switch (v.getId()) {
                case R.id.btnGuardar:
                    boolean completo = true;
                    if(txtTitulo.getText().toString().equals("")) {
                        txtTitulo.setError("Introduce el Nombre");
                        completo = false;
                    }
                    if(txtContenido.getText().toString().equals("")) {
                        txtContenido.setError("Introduce el telefono principal");
                        completo = false;
                    }
                    if(completo){
                        Notas nNotas = new Notas();
                        nNotas.setTitulo(txtTitulo.getText().toString());
                        nNotas.setContenido(txtContenido.getText().toString());
                        nNotas.setFecha(fecha);
                        nNotas.setFavorite(cbkFavorite.isChecked() ? 1 : 0);
                        nNotas.setIdUser(idUser);

                        if(savedNotas == null) {
                            php.insertarNotas(nNotas);
                            Toast.makeText(NotasActivity.this,"Exito",Toast.LENGTH_SHORT).show();
                            limpiar();
                        }else{
                            php.actualizarNotas(nNotas, id);
                            Toast.makeText(NotasActivity.this, "Exito",Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    }
                    break;
                case R.id.btnLimpiar:
                    limpiar();
                    break;
                case R.id.btnListar:
                    Intent intent = new Intent(NotasActivity.this, ListaNotasActivity.class);
                    intent.putExtra("idUsuario",idUser); ////Para enviar dato a otra actividad
                    limpiar();
                    startActivityForResult(intent, 0);
                    break;
            }
        }else{

            Toast.makeText(NotasActivity.this,"Se necesita tener conexion a internet",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void initComponents() {
        this.php = new ProcesosPHP();
        php.setContext(this);

        this.txtTitulo = findViewById(R.id.txtTitulo);
        this.txtContenido = findViewById(R.id.txtContenido);
        this.cbkFavorite = findViewById(R.id.cbkFavorito);
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.btnListar = findViewById(R.id.btnListar);
        this.btnLimpiar = findViewById(R.id.btnLimpiar);
        savedNotas = null;
    }

    public void setEvents() {
        this.btnGuardar.setOnClickListener(this);
        this.btnListar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return ni !=  null && ni.isConnected();
    }
    public void limpiar() {
        savedNotas = null;
        txtTitulo.setText("");
        txtContenido.setText("");
        cbkFavorite.setChecked(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if(intent != null) {
            Bundle oBundle = intent.getExtras();
            if(Activity.RESULT_OK == resultCode) {
                Notas nota = (Notas) oBundle.getSerializable("notas");
                savedNotas = nota;
                id = nota.getID();
                txtTitulo.setText(nota.getTitulo());
                txtContenido.setText(nota.getContenido());
                if(nota.getFavorite() > 0) {
                    cbkFavorite.setChecked(true);
                }
            }else{
                limpiar();
            }
        }
    }

    public void consultarUsuarioWebService() {
        //Toast.makeText(this.getActivity(), "Usuario y Contraseña: " + usuario + password, Toast.LENGTH_SHORT).show();
        String url = "http://mextrafi.leandevware.com.mx/wsConsultarUsuario.php?usuario="+usuario+"&password="+password;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(NotasActivity.this, "Datos no encontrados", Toast.LENGTH_SHORT).show();
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        //Toast.makeText(NotasActivity.this, "Recivo: "+response, Toast.LENGTH_SHORT).show();
        Usuarios miUsuario = new Usuarios();
        JSONArray json = response.optJSONArray("usuario");
        JSONObject jsonObject = null;

        try {
            jsonObject = json.getJSONObject(0);
            miUsuario.setID(jsonObject.optInt("id_usuario"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        idUser = miUsuario.getID();
    }

    //Metodo para mostrar y ocultar menu
    public  boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.overflowperfil, menu);

        return true;
    }

    //Metodo para asignar funciones al menu
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.perfilItem1) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
