package com.example.mextrafi_transitos;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

public class SplashActivity extends AppCompatActivity {

    private final int DURACION_SPLASH = 2000;
    private ProgressBar progressBar;
    private ObjectAnimator animator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = (ProgressBar) findViewById(R.id.circularProgress);

        //instanciamos el animador; Construye y devuelve un ObjectAnimator que anima.
        animator = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);
        mostrarProgress();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable(){
            public void run(){
                SharedPreferences preferences = getSharedPreferences("preferenciasLogin", Context.MODE_PRIVATE);
                boolean sesion = preferences.getBoolean("sesion", false);

                if(sesion){
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    animator.cancel();
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(SplashActivity.this, loginActivity.class);
                    animator.cancel();
                    startActivity(intent);
                    finish();
                }
            };
        }, DURACION_SPLASH);
    }

    private void mostrarProgress(){
        //agregamos el tiempo de la animacion a mostrar
        animator.setDuration(4000);
        animator.setInterpolator(new DecelerateInterpolator());
        //iniciamos el progressbar
        animator.start();
    }
}
