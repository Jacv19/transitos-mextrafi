package com.example.mextrafi_transitos.Interfaces;

public interface ComunicaFragments {

    public void iniciarMulta();
    public void mostrarNoticias();
    public void mostrarLeandevware();

    public void salirMextrafi();
    public void cerrarSesion();

    public void abrirNotas();

}
