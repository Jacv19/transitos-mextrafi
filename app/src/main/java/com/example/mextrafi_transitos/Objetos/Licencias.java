package com.example.mextrafi_transitos.Objetos;

import java.io.Serializable;

public class Licencias implements Serializable {
    private int ID;
    private String numero;
    private int tipo;
    private int puntos;

    public Licencias() {

    }

    public Licencias(String numero, int tipo, int puntos) {
        this.numero = numero;
        this.tipo = tipo;
        this.puntos = puntos;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
}