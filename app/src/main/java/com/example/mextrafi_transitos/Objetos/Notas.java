package com.example.mextrafi_transitos.Objetos;

import java.io.Serializable;

public class Notas implements Serializable {
    private int ID;
    private String titulo;
    private String contenido;
    private String fecha;
    private int favorite;
    private int idUser;

    public Notas() {

    }

    public Notas(String titulo, String contenido, String fecha, int favorite, int idUser) {
        this.titulo = titulo;
        this.contenido = contenido;
        this.fecha = fecha;
        this.favorite = favorite;
        this.favorite = idUser;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}