package com.example.mextrafi_transitos.Objetos;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener {

    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Notas> notas = new ArrayList<Notas>();
    private String serverip = "https://mextrafi.leandevware.com.mx/";

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    public void insertarNotas(Notas n) {
        String url = serverip + "wsNotaRegistrar.php?titulo="+n.getTitulo()
                +"&contenido="+n.getContenido()
                +"&fecha="+n.getFecha()
                +"&favorite="+n.getFavorite()
                +"&idUser="+n.getIdUser();

        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void actualizarNotas(Notas n, int id) {
        String url = serverip + "wsNotaActualizar.php?_ID="+id
                +"&titulo="+n.getTitulo()
                +"&contenido="+n.getContenido()
                +"&fecha="+n.getFecha()
                +"&favorite="+n.getFavorite()
                +"&idUser="+n.getIdUser();

        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void borrarNotas(int id) {
        String url = serverip + "wsNotaEliminar.php?_ID="+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {

    }

}
