package com.example.mextrafi_transitos.Objetos;

import java.io.Serializable;

public class Usuarios implements Serializable {
    private int ID;
    private String nombre;
    private String direccion;
    private int numLic;
    private int telefono;
    private String email;

    public Usuarios() {

    }

    public Usuarios(String nombre, String direccion, int numLic, int telefono, String email) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.numLic = numLic;
        this.telefono = telefono;
        this.email = email;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNumLic() {
        return numLic;
    }

    public void setNumLic(int numLic) {
        this.numLic = numLic;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
