package com.example.mextrafi_transitos.ui.infracciones;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.mextrafi_transitos.R;

public class InfraccionesFragment extends Fragment {

    private InfraccionesViewModel infraccionesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        infraccionesViewModel = ViewModelProviders.of(this).get(InfraccionesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_infraciones, container, false);
        final TextView textView = root.findViewById(R.id.text_infracion);

        infraccionesViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

}