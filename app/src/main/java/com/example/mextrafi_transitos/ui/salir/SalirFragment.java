package com.example.mextrafi_transitos.ui.salir;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.mextrafi_transitos.Interfaces.ComunicaFragments;
import com.example.mextrafi_transitos.R;

public class SalirFragment extends Fragment {

    private SalirViewModel salirViewModel;
    private CardView salir, cerrar;
    ComunicaFragments interfaceComunicaFragments;
    Activity activity;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        salirViewModel = ViewModelProviders.of(this).get(SalirViewModel.class);
        View root = inflater.inflate(R.layout.fragment_salir, container, false);
        final TextView textView = root.findViewById(R.id.text_salir);

        salir = (CardView) root.findViewById(R.id.cardSalir);
        cerrar = (CardView) root.findViewById(R.id.cardCerrarSesion);

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceComunicaFragments.salirMextrafi();
            }
        });

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceComunicaFragments.cerrarSesion();
            }
        });

        salirViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof Activity) {
            activity = (Activity) context;
            interfaceComunicaFragments = (ComunicaFragments) activity;
        }
    }
}