package com.example.mextrafi_transitos.ui.perfil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PerfilViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PerfilViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("BIENVENIDO A TU PERFIL");
    }

    public LiveData<String> getText() {
        return mText;
    }
}