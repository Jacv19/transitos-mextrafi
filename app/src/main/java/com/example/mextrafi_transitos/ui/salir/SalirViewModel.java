package com.example.mextrafi_transitos.ui.salir;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SalirViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SalirViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Elija La Opcion Que Busca");
    }

    public LiveData<String> getText() {
        return mText;
    }
}