package com.example.mextrafi_transitos.ui.infracciones;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InfraccionesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public InfraccionesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("LISTA DE INFRACCIONES");
    }

    public LiveData<String> getText() {
        return mText;
    }
}