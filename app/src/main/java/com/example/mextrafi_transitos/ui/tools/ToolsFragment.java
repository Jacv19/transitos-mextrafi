package com.example.mextrafi_transitos.ui.tools;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.mextrafi_transitos.Interfaces.ComunicaFragments;
import com.example.mextrafi_transitos.R;

public class ToolsFragment extends Fragment {

    private ToolsViewModel toolsViewModel;
    private CardView notas;
    ComunicaFragments interfaceComunicaFragments;
    Activity activity;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        toolsViewModel = ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);

        notas = (CardView) root.findViewById(R.id.cardNotas);

        notas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceComunicaFragments.abrirNotas();
            }
        });

        toolsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof Activity) {
            activity = (Activity) context;
            interfaceComunicaFragments = (ComunicaFragments) activity;
        }
    }
}