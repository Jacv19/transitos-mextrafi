package com.example.mextrafi_transitos.ui.perfil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mextrafi_transitos.Interfaces.ComunicaFragments;
import com.example.mextrafi_transitos.MainActivity;
import com.example.mextrafi_transitos.Objetos.Licencias;
import com.example.mextrafi_transitos.Objetos.Usuarios;
import com.example.mextrafi_transitos.R;
import com.example.mextrafi_transitos.RecuperarActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PerfilFragment extends Fragment implements  Response.Listener<JSONObject>
        ,Response.ErrorListener{

    private PerfilViewModel perfilViewModel;
    private TextView lblNombre, lblUsuario, lblDireccion, lblTelefono, lblEmail;
    //private TextView lblNumero, lblTipo, lblPuntos;
    private String usuario, password, nombre, direccion, telefono, email, numero, tipos, puntos;
    private int numLic, tel, tipo, punto;

    ProgressDialog progreso;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    ComunicaFragments interfaceComunicaFragments;
    Activity activity;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        perfilViewModel = ViewModelProviders.of(this).get(PerfilViewModel.class);
        View root = inflater.inflate(R.layout.fragment_perfil, container, false);
        final TextView textView = root.findViewById(R.id.text_perfil);

        lblNombre = (TextView) root.findViewById(R.id.lblPerfilNombre);
        lblUsuario = (TextView) root.findViewById(R.id.lblPerfilUser);
        lblDireccion = (TextView) root.findViewById(R.id.lblPerfilDireccion);
        lblTelefono = (TextView) root.findViewById(R.id.lblPerfilTelefono);
        lblEmail = (TextView) root.findViewById(R.id.lblPerfilEmail);
        /*lblNumero = (TextView) root.findViewById(R.id.lblPerfilNumLicencia);
        lblPuntos = (TextView) root.findViewById(R.id.lblPerfilPntLicencia);
        lblTipo = (TextView) root.findViewById(R.id.lblPerfilTipLicencia);*/
        request = Volley.newRequestQueue(getContext());

        SharedPreferences preferences = this.getActivity().getSharedPreferences("preferenciasLogin", Context.MODE_PRIVATE);
        usuario = preferences.getString("usuario", "");
        password = preferences.getString("password", "");

        consultarUsuarioWebService();

        perfilViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    public void consultarUsuarioWebService() {
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();

        //Toast.makeText(this.getActivity(), "Usuario y Contraseña: " + usuario + password, Toast.LENGTH_SHORT).show();
        String url = "http://mextrafi.leandevware.com.mx/wsConsultarUsuario.php?usuario="+usuario+"&password="+password;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void consultarLicenciaWebService() {
        /*progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();*/
        //Toast.makeText(this.getActivity(), "Numero Licencia: " + numLic, Toast.LENGTH_SHORT).show();
        String url = "http://mextrafi.leandevware.com.mx/wsConsultarLicencia.php?licencia="+numLic;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progreso.hide();
        Toast.makeText(this.getActivity(), "Datos no encontrados", Toast.LENGTH_SHORT).show();
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        progreso.hide();
        Toast.makeText(this.getActivity(), "Datos: "+response, Toast.LENGTH_SHORT).show();

        Usuarios miUsuario = new Usuarios();
        JSONArray json = response.optJSONArray("usuario");
        JSONObject jsonObject = null;

        try {
            jsonObject = json.getJSONObject(0);
            miUsuario.setNombre(jsonObject.optString("nombre_usuario"));
            miUsuario.setDireccion(jsonObject.optString("direccion_usuario"));
            miUsuario.setNumLic(jsonObject.optInt("numero_licencia_id"));
            miUsuario.setTelefono(jsonObject.optInt("telefono_usuario"));
            miUsuario.setEmail(jsonObject.optString("email_usuario"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(this.getActivity(), "Datos: "+miUsuario.getNombre(), Toast.LENGTH_SHORT).show();
        nombre = miUsuario.getNombre();
        direccion = miUsuario.getDireccion();
        tel = miUsuario.getTelefono();
        telefono = String.valueOf(tel);
        email = miUsuario.getEmail();
        numLic = miUsuario.getNumLic();
        lblNombre.setText(nombre);
        lblUsuario.setText(usuario);
        lblDireccion.setText(direccion);
        lblTelefono.setText(telefono);
        lblEmail.setText(email);

        /*Licencias miLicencia = new Licencias();
        JSONArray jsonlic = response.optJSONArray("licencia");
        JSONObject jsonObjectlic = null;
        try {
            jsonObjectlic = jsonlic.getJSONObject(0);
            miLicencia.setNumero(jsonObjectlic.optString("numero_licencia"));
            miLicencia.setTipo(jsonObjectlic.optInt("tipo_licencia_id"));
            miLicencia.setPuntos(jsonObjectlic.optInt("puntos_licencia"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        numero = miLicencia.getNumero();
        tipo = miLicencia.getTipo();
        tipos = String.valueOf(tipo);
        punto = miLicencia.getPuntos();
        puntos = String.valueOf(punto);
        lblNumero.setText(numero);
        lblTipo.setText(tipos);
        lblPuntos.setText(puntos);*/
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof Activity) {
            activity = (Activity) context;
            interfaceComunicaFragments = (ComunicaFragments) activity;
        }
    }

    public void limpiar() {
        lblNombre.setText("");
        lblUsuario.setText("");
        lblDireccion.setText("");
        lblTelefono.setText("");
        lblEmail.setText("");
    }
}